//
//  main.m
//  GoCI
//
//  Created by Chao on 9/24/15.
//  Copyright © 2015 Chao. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
