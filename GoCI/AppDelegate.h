//
//  AppDelegate.h
//  GoCI
//
//  Created by Chao on 9/24/15.
//  Copyright © 2015 Chao. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

